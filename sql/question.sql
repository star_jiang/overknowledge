CREATE TABLE `overknowledge`.`question` (
  `id` VARCHAR(45) NOT NULL COMMENT 'unique uuid',
  `title` VARCHAR(45) NULL,
  `content` LONGTEXT NULL,
  `created_timestamp` INT NULL,
  `last_modified_timestamp` BIGINT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC));
  
/* Seed data*/
INSERT INTO `overknowledge`.`question` (`title`, `content`, `created_timestamp`, `last_modified_timestamp`) VALUES ('第一个问题', 'No content...', '1470850887580', '1470850887580');

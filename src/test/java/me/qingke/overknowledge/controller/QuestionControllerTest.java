package me.qingke.overknowledge.controller;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.http.HttpStatus;
import org.springframework.ui.ModelMap;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import me.qingke.overknowledge.model.Question;
import me.qingke.overknowledge.service.QuestionService;
import util.Status;

public class QuestionControllerTest {
    @Mock
    QuestionService service;

    @InjectMocks
    QuestionController questionController;

    @Spy
    List<Question> questions = new ArrayList<Question>();

    @Spy
    ModelMap model;

    @BeforeClass
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void listQuestions() {
        // Mockito.when(service.findAll()).thenReturn(questions);
        Assert.assertEquals(questionController.listQuestions().getHttpStatus(), HttpStatus.OK);
        // List<Question> questions = (List<Question>)
        // questionController.listQuestions(model).getData();
        // Assert.assertTrue(questions.size() > 0);
        // Mockito.verify(service, Mockito.atLeastOnce()).findAll();
    }
}

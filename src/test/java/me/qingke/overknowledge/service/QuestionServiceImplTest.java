package me.qingke.overknowledge.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import me.qingke.overknowledge.dao.QuestionDao;
import me.qingke.overknowledge.model.Question;

public class QuestionServiceImplTest {
    @Mock
    QuestionDao dao;
     
    @InjectMocks
    QuestionServiceImpl questionService;
     
    @Spy
    List<Question> questions = new ArrayList<Question>();
     
    @BeforeClass
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        questions = getQuestionList();
    }
    
    @Test
    public void findAll(){
        //Mockito.when(dao.findAll()).thenReturn(questions);
        Assert.assertEquals(questionService.findAll(), dao.findAll());
    }
    
    public List<Question> getQuestionList() {
        return Collections.EMPTY_LIST;
    }
}
